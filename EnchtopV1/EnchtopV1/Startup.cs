﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EnchtopV1.Startup))]
namespace EnchtopV1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
