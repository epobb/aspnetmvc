﻿using EnchtopV1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EnchtopV1.Controllers
{
    public class BasketController : Controller
    {
        // GET: Basket
        public ActionResult Index()
        {
            var basket = Session["basket"] as Basket ?? new Basket();
            return View(basket);
        }

        public ActionResult Count()
        {
            var basket = Session["basket"] as Basket ?? new Basket();
            return PartialView(basket);
        }
    }
}