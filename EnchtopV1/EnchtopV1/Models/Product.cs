﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EnchtopV1.Models
{
    public class Product
    {
        public int ID { get; set; }
        [Display(Name="Nom")]
        public string Name { get; set; }
        public string Description { get; set; }
        [Display(Name = "Prix")]
        public decimal Price { get; set; }
        public string ImageName { get; set; }
        public Category Category { get; set; }
        public int? CategoryID { get; set; }
    }

    public class Category
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class ShopInitializer : DropCreateDatabaseIfModelChanges<ShopFactory>
    {
        protected override void Seed(ShopFactory context)
        {
            context.Products.Add(new Product() { Name = "Yoghurt", Description = "This creamy one will melt you", Price=5.4M, ImageName="yaourt" });
            context.Products.Add(new Product() { Name = "Cleaning logtion", Description = "Nothing gets in its way", Price = 64M, ImageName="spray" });
            context.Products.Add(new Product() { Name = "Banana", Description = "Hungry? That's going to get you satisfied", Price = 3M, ImageName="banane" });
        }
    }

    public class ShopFactory : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }


        public ShopFactory()
        {
            Database.SetInitializer(new ShopInitializer());
        }
    }
}