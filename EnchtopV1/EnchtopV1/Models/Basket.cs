﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnchtopV1.Models
{
    public class Basket : List<BasketItem>
    {
        public void AddOrIncrement(Product product)
        {
            var matchingItem = this.Where(p => p.Product.ID == product.ID).FirstOrDefault();

            if (matchingItem == null)
            {
                this.Add(new BasketItem() { Product = product, Quantity = 1 });
            }
            else
            {
                matchingItem.Quantity++;
            }
        }
    }

    public class BasketItem
    {
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}