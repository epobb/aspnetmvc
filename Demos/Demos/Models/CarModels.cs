﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Demos.Models
{
    public class Car
    {
        public int ID { get; set; }

        [Required]
        [RegularExpression("..+")]
        public string Model { get; set; }

        [Range(10, 300)]
        public double MaxSpeed { get; set; }
    }

    public class GarageFactory : DbContext
    {
        public DbSet<Car> Cars { get; set; }

        public GarageFactory()
        {
            Database.SetInitializer(new GarageInitializer());
        }
    }

    public class GarageInitializer : DropCreateDatabaseIfModelChanges<GarageFactory>
    {
        protected override void Seed(GarageFactory context)
        {
            context.Cars.Add(new Car() { Model = "Rabbit", MaxSpeed = 300 });
            context.Cars.Add(new Car() { Model = "Turtle", MaxSpeed = 150 });
        }
    }
}
