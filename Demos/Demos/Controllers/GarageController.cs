﻿using Demos.Models;
using Demos.ViewModels.Garage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demos.Controllers
{
    public class GarageController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult CarsList()
        {
            var factory = new GarageFactory();
            var viewModel = new CarsListViewModel(factory.Cars);
            return View(viewModel);
        }
    }
}