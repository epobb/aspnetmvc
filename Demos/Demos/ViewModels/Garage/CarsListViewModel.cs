﻿using Demos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Demos.ViewModels.Garage
{
    public class CarsListViewModel
    {
        public CarsListViewModel(IEnumerable<Car> cars)
        {
            CarsList = cars.Select(
                c => new SelectListItem() { Text = c.Model }
            );
            FastestCar = cars.OrderByDescending(c => c.MaxSpeed).FirstOrDefault();
        }

        public IEnumerable<SelectListItem> CarsList { get; private set; }
        public Car FastestCar { get; set; }
    }
}