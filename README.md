Free source code for [Learn ASP.NET MVC book](http://leanpub.com/aspnetmvc) demos and do-it-yourself

# Get the book

Buy the book here:

* [paperback on Amazon](http://www.amazon.com/t/dp/132648303X), [paperback on Lulu](http://www.lulu.com/shop/arnaud-weil/learn-aspnet-mvc/paperback/product-22455747.html)
* [ebook on Amazon](http://www.amazon.com/Learn-ASP-NET-MVC-coding-Visual-ebook/dp/B018O9QG3S), [ebook on Leanpub](https://leanpub.com/aspnetmvc)

# Get the source code

Just `git clone` that repository. If that sounds obscure to you, click the "Downloads" link at the left of this window.

# Book TOC

Introduction

1. Creating our Web Site 
2. ASP.NET MVC inner workings
2.1 Principles 
2.2 View 

3. Create an application and modify the home page
3.1 Do-it-yourself 1 - Create the application 
3.2 Do-it-yourself 2 - Change the home page 

4. Razor
4.1 Razor syntax 
4.2 Layout views 
4.3 Do-it-yourself 4 - Remove some links 
4.4 Helpers 

5. Understanding ASP.NET MVC
5.1 Flashback 
5.2 Routing 
5.3 Controllers 
5.4 Be lazy 
5.5 Let’s go all the way 

6. Typing things up
6.1 The problem with ViewBag and ViewData 
6.2 Using and typing the model 
6.3 Conventions and simplicity: introducing the ViewModel 
6.4 Entity Framework models 
6.5 Do-it-yourself 5 - Create the Product model and DbContext 
6.6 Do-it-yourself 6 - Add code that creates a database with some products 
6.7 Do-it-yourself 7 - Display a products list 

7. Updating server data
7.1 Action parameters 
7.2 Word of caution about URLs 
7.3 Do-it-yourself 8 - Display product details 
7.4 HTTP Post parameters 
7.5 Passing a full blown object 
7.6 Sit and watch - Basic product calculator 
7.7 Do-it-yourself 9 - Add a search box to the products list 

8. Updating data scenario
8.1 Steps 
8.2 Controller 
8.3 Automated generation of controller and views 
8.4 Do-it-yourself 10 - Create the products management back-office 

9. Doing more with controllers and actions
9.1 Actions can generate more than views 
9.2 Do-it-yourself 11 - Add images to the products 
9.3 Input validation 

10. Basic security
10.1 Preventing Cross-Site Scripting 
10.2 Rejecting extra fields 
10.3 Identifying users 
10.4 Do-it-yourself 12 - Secure the back-office 

11. Going further
11.1 Deploying your site 
11.2 Creating Razor helpers 
11.3 Partial views 
11.4 Display and edit templates 

12. What is ASP.NET MVC and why use it
12.1 What is it ? 
12.2 Why use it ? 
12.3 Competing technologies 

Do-it-yourself Cheat Sheet
Do-it-yourself 1 - Create the application - correction 
Do-it-yourself 2 - Change the home page - correction 
Do-it-yourself 3 - Add code to the home page - correction 
Do-it-yourself 4 - Remove some links - correction 
Do-it-yourself 5 - Create the Product model and DbContext - correction 
Do-it-yourself 6 - Add code that creates a database with some products - correction 
Do-it-yourself 7 - Display a products list - correction 
Do-it-yourself 8 - Display product details - correction 
Do-it-yourself 9 - Add a search box to the products list - correction 
Do-it-yourself 10 - Create the products management back-office - correction 
Do-it-yourself 11 - Add images to the products - correction 
Do-it-yourself 12 - Secure the back-office - correction 

Définitions
Bootstrap 
Dynamic object 
Entity Framework 
Project 
Solution 
Solution Explorer